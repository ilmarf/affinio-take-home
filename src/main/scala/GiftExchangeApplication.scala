import scala.collection.mutable.ArrayBuffer
import scala.util.Random


/**
  *
  * This class represents an application instance which objective is to solve a simple problem:
  * Given a list of people's names, organize a gift exchange such that each person is randomly assigned someone else to give a gift to, and someone to receive a gift from.
  * Caveat: The person cannot receive a gift from the same person to whom they are gifting one.
  *
  * Example
  * input: "Phil, Stephen, Ardi, Quan, Joel"
  * output: "Phil gives Joel a gift. Joel gives Quan a gift. Quan gives Stephen a gift. Stephen gives Ardi a gift. Ardi gives Phil a gift"
  *
  * Having said that, this class implements a solution to the above problem by using one-cyclic-graph representation with the following properties:
  *   - If A gives a gift to B then A has a giving relation with B. The total number of relation is the same amount of participants
  *   - For the graph to be cyclic then all the participants must receive exact one gift but also give one gift, represented by the giving relations
  *   - A participant should not receive a gift from the person to whom is giving the gift to.
  *   - The minimum quantity of participants must be at least 3, so the rule above wont be broken
  *
  * In this implementation the one-cyclic-graph is represented by a simple one dimension string array containing each participant.
  * The array positions represent which participant will give gift to the another one by relying on the position for that.
  *
  * Given n participants, a n-size array will the randomly shuffled with the participants where:
  *
  * For a given participant lying on array index i, then the index of its gift recipient is:
  *
  * java.lang.Math.floorMod(i + 1, n)
  *
  * Likewise, for a given participant lying on array index i, then the index of its gift sender is:
  *
  * java.lang.Math.floorMod(i - 1, n)
  *
  * @param participants A set of participants who will exchange gifts
  */
class GiftExchangeApplication(val participants: Set[String]) {

  private var participantArray: Array[String] = _

  require(participants != null, "The list of participants should not never be null.")

  require(participants.size >= 3, "The list of participants should contain at least 3. There wont be any surprise if the size is less than 3, right?")

  this.participantArray = participants.to[Array]


  /**
    * The present implementation randomly shuffles the instance participants and organize them to the gift exchange
    */
  def organizeGiftExchange(): Unit = {

    this.shuffleParticipantArray( )

  }


  /**
    * Simple shuffle function which swaps the participant array elements randomly, giving each of the element unbiased
    * chance to fill each position
    */
  private def shuffleParticipantArray(): Unit = {

    val input = this.participantArray

    val maxIndex = input.length - 1

    for (i <- maxIndex to 1 by -1) {

      val randomIndex: Int = Random.nextInt(i)

      val oldLastIndexValue: String = input(i)

      val newLastIndexValue: String = input(randomIndex)

      input(i) = newLastIndexValue

      input(randomIndex) = oldLastIndexValue

    }

  }


  /**
    * This function builds a friendly representation of gift exchange so it is easier to transverse the relations
    * between the participants
    *
    * @return A collection of ExchangeRelation indicating the relation between a gift sender and a receiver
    **/
  def buildGifExchangeRelationRepresentation(): ArrayBuffer[ExchangeRelation] = {

    var result: ArrayBuffer[ExchangeRelation] = ArrayBuffer[ExchangeRelation]( )

    val totalSize = participantArray.length

    for (i <- 0 until totalSize) {

      val givingGiftParticipant = participantArray(i)

      val receivingGiftParticipant = participantArray(java.lang.Math.floorMod(i + 1, totalSize))

      result += new ExchangeRelation(givingGiftParticipant, receivingGiftParticipant)
    }

    result

  }


}

/**
  * Simple application which command line interface takes a list of participants as an argument, organize the gift exchange and prints on the
  * console the exchange model
  */
object GiftExchangeApplication {


  def main(args: Array[String]): Unit = {

    val giftExchangeApplication = new GiftExchangeApplication(args.toSet)

    giftExchangeApplication.organizeGiftExchange( )

    giftExchangeApplication.buildGifExchangeRelationRepresentation( )

    printRelations(giftExchangeApplication)

  }


  def printRelations(input: GiftExchangeApplication): Unit = {

    val participantArray: Array[String] = input.participantArray

    val totalSize = participantArray.length

    println("Listing giving relations:")

    for (i <- 0 until totalSize) {

      println(s"Index $i - " + participantArray(i) + " will give gift to " + participantArray(java.lang.Math.floorMod(i + 1, totalSize)))


    }

    println("Listing receiving relations:")

    for (i <- 0 until totalSize) {

      println(s"Index $i - " + participantArray(i) + " will receive gift from " + participantArray(java.lang.Math.floorMod(i - 1, totalSize)))
    }

  }

}
